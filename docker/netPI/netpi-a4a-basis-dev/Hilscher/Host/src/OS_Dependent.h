/**************************************************************************************

Copyright (c) Hilscher Gesellschaft fuer Systemautomation mbH. All Rights Reserved.

***************************************************************************************

  $Id: OS_Dependent.h $:

  Description:
    OS Dependent function declaration. These functions must be implemented.

  Changes:
    Date        Description
    -----------------------------------------------------------------------------------
    2013-02-13  initial version

**************************************************************************************/

#ifndef __OS_DEPENDENT__H
#define __OS_DEPENDENT__H

#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

#define OS_EVENT_SIGNALLED  0
#define OS_EVENT_TIMEOUT    1

#define OS_SEM_SIGNALLED    0
#define OS_SEM_TIMEOUT      1

#define OS_INFINITE_TIMEOUT 0xFFFFFFFF

#define NXT_MIN(a,b) ((a < b)? (a) : (b))
#define NXT_MAX(a,b) ((a > b)? (a) : (b))

void*    OS_Memalloc(uint32_t ulSize);
void*    OS_Memrealloc(void* pvBuffer, uint32_t ulSize);
void     OS_Memfree(void* pvMem);

void     OS_Memset(void* pvMem, unsigned char bFill, uint32_t ulSize);
void     OS_Memcpy(void* pvDest, void* pvSrc, uint32_t ulSize);
int      OS_Memcmp(void* pvBuf1, void* pvBuf2, uint32_t ulSize);
void     OS_Memmove(void* pvDest, void* pvSrc, uint32_t ulSize);

uint32_t OS_GetMilliSecCounter(void);
void     OS_Sleep(uint32_t ulSleepTimeMs);

void*    OS_CreateLock(void);
void     OS_EnterLock(void* pvLock);
void     OS_LeaveLock(void* pvLock);
void     OS_DeleteLock(void* pvLock);

void*    OS_CreateEvent(void);
void     OS_SetEvent(void* pvEvent);
void     OS_ResetEvent(void* pvEvent);
void     OS_DeleteEvent(void* pvEvent);
uint32_t OS_WaitEvent(void* pvEvent, uint32_t ulTimeout);

void*    OS_CreateSemaphore(uint32_t ulInitialCount);
void     OS_PutSemaphore(void* pvSem, uint32_t ulCount);
void     OS_DeleteSemaphore(void* pvSem);
uint32_t OS_WaitSemaphore(void* pvSem, uint32_t ulTimeout);

int      OS_Strcmp(const char* pszBuf1, const char* pszBuf2);
int      OS_Strnicmp(const char* pszBuf1, const char* pszBuf2, uint32_t ulLen);
int      OS_Strlen(const char* szText);
char*    OS_Strncpy(char* szDest, const char* szSource, uint32_t ulLen);
uint32_t OS_Strcspn( char* szSearchStr, char* szFindStr);
char*    OS_Strncat(char* szDest, char* szSrc, uint32_t ulDstSize);

#ifdef NXT_MULTITHREADED
int32_t  OS_CreateThread( void* pvUserParam, uint32_t ulParamSize);
int32_t  OS_DeleteThread( void* pvUserParam);

void*    OS_CreateInterLockedVariable( void);
void     OS_DeleteInterLockedVariable( void* pvVal);
uint32_t OS_InterLockedSet           ( void* pvVal, uint32_t ulVal);
uint32_t OS_InterLockedInc           ( void* pvVal);
uint32_t OS_InterLockedDec           ( void* pvVal);
uint32_t OS_InterLockedRead          ( void* pvVal);
#endif

#ifdef __cplusplus
}
#endif


#endif /* __OS_DEPENDENT__H */
