netXTransport-Toolkit Version History:
======================================

V1.0.3.0 (12.08.2015)
------------------------
 - Added:

 - Change:

 - Bugfix:
    * netXTransportStart() stops to load connectors if one fails to open

V1.0.2.0 (24.07.2015)
------------------------
 - Added:

 - Change:
    * Update toolkit to use shared Marshaller header files

 - Bugfix:
    * Fix data types to be usable on 64bit systems

V1.0.1.0 (04.03.2014)
------------------------
 - Added:
    * Add support for parallel services (NXT_MULTITHREADED)
      (NOTE: requires implementation of additional user- and OS- functions)
    * Add support for buffer pre-allocation (PACKET_PREALLOCATE)
    * cifX API: Add support for xDriverRestartDevice()
    * cifX API: Preprocessor macro NXT_DISABLE_DRIVERHANDLECHECK introduced to disable 
      driver handle check (required to maintain compatibility to released 
      hilscher standard software i.e. cifXTest)
    * device reset: reconnect of connection only executed if endpoint requires it
 - Change:
    * Improved device access and reconnect (skip reconnect if already in progress)
    * Ommit interface designation in board name by allowing nameless interface
    * Redundant OS dependent function OS_Strvsprintf removed as it may causes 
      high memory usage on low end systems
    * Declaration of OS Abstraction functions OS_Strncat/OS_Strncpy changed 
      to conform to ANSI C 
 - Bugfix:
    * Calling convention ommitted in netXTransport function pointer definition
    * cifX API: Access of the receivce packet queue (rcX-Packet layer) was not synchronized
    * cifX API: xSysdeviceGetMBXState()/xChannelGetMBXState() did not return the 
      correct value (rcX-packet layer)

V1.0.0.0 (14.08.2013)
------------------------
- version update (formal reason)

V0.9.0.0 (13.08.2013)
------------------------
- initial version
  (created from netXTRansport DLL V0.10.0.0 implementation)