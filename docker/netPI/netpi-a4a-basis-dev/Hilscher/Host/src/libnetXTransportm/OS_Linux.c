/**************************************************************************************

   Copyright (c) Hilscher GmbH. All Rights Reserved.

 **************************************************************************************

   Filename:
    $Id: OS_win32.c 3194 2011-12-14 16:36:28Z Robert $
   Last Modification:
    $Author: Robert $
    $Date: 2011-12-14 17:36:28 +0100 (Mi, 14 Dez 2011) $
    $Revision: 3194 $

   Targets:
     win32        : yes

   Description:
    windows OS abstraction for netXTransport toolkit

   Changes:

     Version   Date        Author   Description
     ----------------------------------------------------------------------------------
      1        23.02.13    SD       initial version

**************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <limits.h> /* for PTHREAD_STACK_MIN */
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <semaphore.h>
#include <errno.h>

#include "OS_Dependent.h"
#include "netXTransport_Errors.h"
#include "netXTransport.h"

/*****************************************************************************/
/*! Create Lock (Usually same as mutex, but does not support timed waiting)
*     \return Handle to created lock                                         */
/*****************************************************************************/
void* OS_CreateLock(void)
{
  pthread_mutexattr_t mta;
  pthread_mutex_t     *mutex;

  #ifdef VERBOSE
    printf("%s() called\n", __FUNCTION__);
  #endif
  pthread_mutexattr_init(&mta);
  if( pthread_mutexattr_settype(&mta, PTHREAD_MUTEX_RECURSIVE) != 0 )
  {
    perror("set mutex recursive");
    return NULL;
  }
  mutex = malloc( sizeof(pthread_mutex_t) );
  if( mutex == NULL )
  {
    perror("allocating memory for mutex");
    return NULL;
  }
  if( pthread_mutex_init(mutex, &mta) != 0 )
  {
    perror("mutex init:");
    goto err_out;
  }
  return mutex;

  err_out:
    free(mutex);
  return NULL;
}

/*****************************************************************************/
/*! Acquire a lock
*     \param pvLock Handle to lock                                           */
/*****************************************************************************/
void OS_EnterLock(void* pvLock)
{
  pthread_mutex_t *mutex = (pthread_mutex_t *) pvLock;

#ifdef VERBOSE_2
  printf("%s() called\n", __FUNCTION__);
#endif

  if( pthread_mutex_lock(mutex) != 0)
  {
    perror("EnterLock failed");
  }
}


/*****************************************************************************/
/*! Release a lock
*     \param pvLock Handle to lock                                           */
/*****************************************************************************/
void OS_LeaveLock(void* pvLock)
{
  pthread_mutex_t *mutex = (pthread_mutex_t *) pvLock;

#ifdef VERBOSE_2
  printf("%s() called\n", __FUNCTION__);
#endif
  if( pthread_mutex_unlock(mutex) != 0)
  {
    perror("LeaveLock failed");
  }
}

/*****************************************************************************/
/*! Delete a lock
*     \param pvLock Handle to lock                                           */
/*****************************************************************************/
void OS_DeleteLock(void* pvLock)
{
  pthread_mutex_t *mutex = (pthread_mutex_t *) pvLock;
#ifdef VERBOSE
  printf("%s() called\n", __FUNCTION__);
#endif
  if( pthread_mutex_destroy(mutex) != 0 )
    perror("DeleteLock failed");

  free(mutex);
}

char* OS_Strvsprintf(char *szDest, uint32_t ulSize, const char *format, ...)
{
  va_list ap;

  va_start(ap, format);
  vsprintf( szDest, format, ap);
  va_end(ap);
  return szDest;
}

char* OS_Strcat(char* szDest, uint32_t ulDstSize, char* szSrc)
{
  return strcat( szDest, szSrc);
}

/*****************************************************************************/
/*! Compare strings
*     \param pszBuf1  String buffer 1
*     \param pszBuf2  String buffer 2
*     \return 0 if strings are equal                                         */
/*****************************************************************************/
int OS_Strcmp(const char* pszBuf1, const char* pszBuf2)
{
  return strcmp(pszBuf1, pszBuf2);
}

/*****************************************************************************/
/*! Copy string to destination buffer
*     \param szText   Destination string
*     \param szSource Source string
*     \param ulLen    Maximum length to copy
*     \return Pointer to szDest                                              */
/*****************************************************************************/
char* OS_Strncpy(char* szDest, const char* szSource, uint32_t ulLen)
{
  return strncpy(szDest, szSource, ulLen);
}

/*****************************************************************************/
/*! Get length of string
*     \param szText  Text buffer
*     \return Length of given string                                         */
/*****************************************************************************/
int OS_Strlen(const char* szText)
{
  return (int)strlen(szText);
}

int OS_Strnicmp(const char* pszBuf1, const char* pszBuf2, uint32_t ulLen) {
  return strncasecmp(pszBuf1, pszBuf2, ulLen);
}

char* OS_Strncat(char* szDest, char* szSrc, uint32_t ulDstSize)
{
  return strncat( szDest, szSrc, ulDstSize);
}

/*****************************************************************************/
/*! Memory allocation
*     \param ulSize Size of block to allocate
*     \return NULL on failure                                                */
/*****************************************************************************/
void* OS_Memalloc(uint32_t ulSize)
{
  return malloc(ulSize);
}

/*****************************************************************************/
/*! Memset
*     \param pvMem   Memory to set
*     \param bFill   Fill byte
*     \param ulSize  Size of the fill block                                  */
/*****************************************************************************/
void OS_Memset(void* pvMem, uint8_t bFill, uint32_t ulSize)
{
  memset(pvMem, bFill, ulSize);
}

/*****************************************************************************/
/*! Memcopy
*     \param pvDest  Destination pointer
*     \param pvSrc   Source pointer
*     \param ulSize  Size to copy                                            */
/*****************************************************************************/
void OS_Memcpy(void* pvBuf1, void* pvBuf2, uint32_t ulSize)
{
  memcpy(pvBuf1, pvBuf2, ulSize);
}

/*****************************************************************************/
/*! Memory resize
*     \param pvMem      Block to resize
*     \param ulNewSize  New size of the block
*     \return NULL on error                                                  */
/*****************************************************************************/
void* OS_Memrealloc(void* pvBuffer, uint32_t ulSize)
{
  return realloc(pvBuffer,ulSize);
}

/*****************************************************************************/
/*! Memcompare wrapper
*     \param pvBuf1  First compare buffer
*     \param pvBuf2  Second compare buffer
*     \param ulSize  Size to compare
*     \return 0 if blocks are equal                                          */
/*****************************************************************************/
int OS_Memcmp(void* pvBuf1, void* pvBuf2, uint32_t ulSize)
{
  return memcmp(pvBuf1, pvBuf2, ulSize);
}

/*****************************************************************************/
/*! Memory de-allocation
*     \param pvMem  Block to free                                            */
/*****************************************************************************/
void OS_Memfree(void* pvMem)
{
  free(pvMem);
}

/*****************************************************************************/
/*! Structure for event handling                                             */
/*****************************************************************************/
struct os_event {
  pthread_mutex_t mutex;           /*!< Mutex to lock access to set, waiting_threads
                                        and cond */
  int             set;             /*!< Protected by mutex. !=0 if event is set */
  pthread_cond_t  cond;            /*!< Condition to signal, if event state has changed,
                                        and a thread is waiting */
};

/*****************************************************************************/
/*! Create event
*     \return Handle to created event                                        */
/*****************************************************************************/
void* OS_CreateEvent(void)
{
  struct os_event     *ev       = malloc( sizeof(*ev) );
  pthread_condattr_t  ev_attr;
  pthread_mutexattr_t ev_mutattr;

#ifdef VERBOSE
  printf("%s() called\n", __FUNCTION__);
#endif
  if( ev == NULL )
  {
    perror("allocating memory for OS_Event");
    return NULL;
  }

  if( pthread_condattr_init( &ev_attr ) != 0 )
  {
    perror("CreateEvent init condattr");
    goto free_out;
  }

  pthread_condattr_setclock(&ev_attr, CLOCK_REALTIME/*CLOCK_MONOTONIC*/);

  if( pthread_cond_init( &(ev->cond), &ev_attr ) != 0 )
  {
    perror("CreateEvent init cond");
    goto free_out;
  }
  if( pthread_mutexattr_init(&ev_mutattr) != 0 )
  {
    perror("WaitEvent mutexattr_init");
    goto free_out;
  }
  if( pthread_mutexattr_setprotocol(&ev_mutattr, PTHREAD_PRIO_INHERIT) != 0 )
  {
    perror("WaitEvent mutexattr_init");
    goto free_out;
  }
  if( pthread_mutex_init(&(ev->mutex), &ev_mutattr) != 0 )
  {
    perror("WaitEvent mutexattr_init");
    goto free_out;
  }

  ev->set             = 0;

  return ev;

free_out:
  free(ev);
  return NULL;
}

/*****************************************************************************/
/*! Signal event
*     \param pvEvent Handle to event                                         */
/*****************************************************************************/
void OS_SetEvent(void* pvEvent)
{
  struct os_event *ev = (struct os_event *) pvEvent;
#ifdef VERBOSE
  printf("%s() called\n", __FUNCTION__);
#endif

  if( ev == NULL )
  {
    fprintf(stderr, "SetEvent, no event given\n");
  } else
  {
    pthread_mutex_lock(&ev->mutex);
    ev->set = 0;

    if (!ev->set)
    {
      /* Check if there are any waiters, and release them appropriately */
      ev->set = 1;
      if( pthread_cond_broadcast(&(ev->cond)) != 0 )
        perror("SetEvent");
    }
    pthread_mutex_unlock(&ev->mutex);
  }
}

/*****************************************************************************/
/*! Reset event
*     \param pvEvent Handle to event                                         */
/*****************************************************************************/
void OS_ResetEvent(void* pvEvent)
{
  struct os_event *ev = (struct os_event *) pvEvent;
#ifdef VERBOSE
  printf("%s() called\n", __FUNCTION__);
#endif
  if( ev == NULL )
  {
    fprintf(stderr, "ResetEvent, no event given\n");
  } else
  {
    //pthread_mutex_lock(&ev->mutex);

    ev->set = 0;

    //pthread_mutex_unlock(&ev->mutex);
  }
}

/*****************************************************************************/
/*! Delete event
*     \param pvEvent Handle to event                                         */
/*****************************************************************************/
void OS_DeleteEvent(void* pvEvent)
{
  struct os_event *ev = (struct os_event *) pvEvent;

#ifdef VERBOSE
  printf("%s() called\n", __FUNCTION__);
#endif

  if( pthread_cond_destroy(&(ev->cond) ) != 0 )
    perror("DeleteEvent cond");
  if( pthread_mutex_destroy(&(ev->mutex) ) != 0 )
    perror("DeleteEvent mut");

  free(ev);
}

/*****************************************************************************/
/*! Calculates timestamp based on realtime clock (returns 0 on success).
*     \param ulTimeout      timeout to add in ms
*     \param ptTimeout      Pointer for returned timestamp
*     \return <0 on failure                                                  */
/*****************************************************************************/
int get_abstime( uint32_t ulTimeout, struct timespec* ptTimeout)
{
  if (ptTimeout == NULL)
          return -1;

  if ( 0 != clock_gettime(CLOCK_REALTIME, ptTimeout))
  {
    perror("WaitEvent gettime failed");
  } else
  {
    ptTimeout->tv_sec  += ulTimeout / 1000;
    ulTimeout           = ulTimeout % 1000;
    ptTimeout->tv_nsec += ulTimeout * 1000 * 1000;
    while (ptTimeout->tv_nsec >= 1000000000) {
      ptTimeout->tv_nsec -= 1000000000;
      ptTimeout->tv_sec++;
    }
    return 0;
  }
  return -1;
}

/*****************************************************************************/
/*! Wait for event
*     \param pvEvent   Handle to event
*     \param ulTimeout Timeout in ms to wait for event
*     \return CIFX_EVENT_SIGNALLED if event was set, CIFX_EVENT_TIMEOUT otherwise */
/*****************************************************************************/
uint32_t OS_WaitEvent(void* pvEvent, uint32_t ulTimeout)
{
  struct os_event *ev       = (struct os_event *) pvEvent;
  unsigned long   ret       = OS_EVENT_TIMEOUT;
  int             fInfinite = 0;
  struct timespec timeout   = {0};

#ifdef VERBOSE
  printf("%s() called\n", __FUNCTION__);
#endif
  pthread_mutex_lock(&ev->mutex);

  if (ulTimeout == OS_INFINITE_TIMEOUT)
    fInfinite = 1;

  if(!ev->set)
  {
    if (fInfinite == 0) {
      get_abstime( ulTimeout, &timeout);
      ret = pthread_cond_timedwait(&ev->cond, &ev->mutex, &timeout);
    } else
    {
      ret = pthread_cond_wait(&ev->cond, &ev->mutex);
    }
  }
  if ((ev->set) || (ret == 0))
  {
    /* We got the event, now reset it */
    ev->set = 0;
    ret = OS_EVENT_SIGNALLED;
  } else
  {
    ret = OS_EVENT_TIMEOUT;
  }

  pthread_mutex_unlock(&ev->mutex);

  return ret;
}

/*****************************************************************************/
/*! Create Semaphore
*     \param ulInitVal
*     \return Pointer to semaphore object                                    */
/*****************************************************************************/
void* OS_CreateSemaphore(uint32_t ulInitVal)
{
  sem_t* ptSem = NULL;
  void*  pvRet = NULL;

  if (NULL != (ptSem = OS_Memalloc(sizeof(sem_t))))
  {
    if (0 == sem_init( ptSem, 0, ulInitVal))
    {
      pvRet = ptSem;
    } else
    {
      OS_Memfree(ptSem);
    }
  }
  return pvRet;
}

/*****************************************************************************/
/*! Deletes Semaphore
*     \param pvSem    Pointer to semaphore object                            */
/*****************************************************************************/
void OS_DeleteSemaphore(void* pvSemaphore)
{
  sem_t* ptSem = (sem_t*)pvSemaphore;

  sem_destroy(ptSem);

  OS_Memfree(ptSem);
}

/*****************************************************************************/
/*! Inc Semaphore
*     \param pvSemaphore    Pointer to semaphore object
*     \param ulCount        value to increment                               */
/*****************************************************************************/
void OS_PutSemaphore(void* pvSemaphore, uint32_t ulCount)
{
  sem_t * ptSem = (sem_t *)pvSemaphore;

  sem_post(ptSem);
}

/*****************************************************************************/
/*! Wait for semaphore
*     \param pvSem   Pointer to semaphore object
*     \param ulTimeout Timeout in ms to wait for semaphore
*     \return OS_SEM_SIGNALLED if semaphore was set, OS_SEM_TIMEOUT otherwise */
/*****************************************************************************/
uint32_t OS_WaitSemaphore(void* pvSemaphore, uint32_t ulTimeout)
{
  uint32_t         ulRet     = OS_SEM_TIMEOUT;
  sem_t*           ptSem     = (sem_t*)pvSemaphore;
  int              fInfinite = 0;
  struct timespec  tTimeout  = {0};
  int ret;

  if (ulTimeout == OS_INFINITE_TIMEOUT)
    fInfinite = 1;

  if (fInfinite == 0) {
    /* get the timestamp */
    get_abstime( ulTimeout, &tTimeout);
    ret = sem_timedwait( ptSem, &tTimeout);
  } else {
    ret = sem_wait( ptSem);
  }
  if (ret == 0)
    ulRet = OS_SEM_SIGNALLED;

  return ulRet;
}

/*****************************************************************************/
/*! Open file for reading
*     \param szFilename   File to open (including path)
*     \param pulFileSize  Returned size of the file in bytes
*     \return Handle to the file, NULL on failure                            */
/*****************************************************************************/
void* OS_FileOpen(char* szFilename, uint32_t * pulFileSize) {
  int         fd;
  struct stat buf;

  fd = open(szFilename, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
  if( fd == -1 )
  {
    return NULL;
  }

  if( fstat(fd, &buf) != 0 )
  {
    perror("fstat failed");
    return NULL;
  }

  *pulFileSize = buf.st_size;

  return fdopen(fd, "w+");
}

/*****************************************************************************/
/*! Read data from file
*     \param pvFile    Handle to the file (acquired by OS_FileOpen)
*     \param ulOffset  Offset to read from
*     \param ulSize    Size to read
*     \param pvBuffer  Buffer to read data into
*     \return number of bytes read                                           */
/*****************************************************************************/
uint32_t OS_FileRead(void* pvFile, uint32_t ulOffset,
                          uint32_t ulSize, void* pvBuffer) {

  return fread(pvBuffer, 1, ulSize, pvFile);
}

/*****************************************************************************/
/*! Close open file
*     \param pvFile    Handle to the file (acquired by OS_FileOpen)          */
/*****************************************************************************/
void OS_FileClose(void* pvFile) {

  if( fclose(pvFile) != 0 )
    perror("FileClose failed");
}

/*****************************************************************************/
/*! Get Millisecond counter value (used for timeout handling)
*     \return Counter value with a resolution of 1ms                         */
/*****************************************************************************/
uint32_t OS_GetMilliSecCounter( void) {
  struct timespec ts_get_milli;
  unsigned int    msec_count;

  if( clock_gettime( CLOCK_MONOTONIC, &ts_get_milli ) != 0 )
  {
    perror("gettime failed");
    return 0;
  }
  msec_count = ts_get_milli.tv_sec * 1000;
  msec_count += ts_get_milli.tv_nsec / 1000 / 1000;

  return msec_count;
}

/*****************************************************************************/
/*! Sleep
*     \param ulSleep Time to sleep in ms                                     */
/*****************************************************************************/
void OS_Sleep(uint32_t ulSleepTimeMs) {
  struct timespec sleeptime;
  struct timespec RemainingTime;
  struct timespec *pRemainingTime = &RemainingTime;
  int    iRet;
  int    iTmpErrno;

  if(ulSleepTimeMs == 0)
  {
    sleeptime.tv_sec = 0;
    sleeptime.tv_nsec = 50000; // 50 usecs
  } else
  {
    sleeptime.tv_sec = ulSleepTimeMs / 1000;
    ulSleepTimeMs -= sleeptime.tv_sec * 1000;
    sleeptime.tv_nsec = ulSleepTimeMs * 1000 * 1000;
  }

  iTmpErrno = errno;
  errno = 0;
  while((iRet = nanosleep(&sleeptime, pRemainingTime)))
  {
    if ((errno == EINTR) && (pRemainingTime != NULL) )
    {
      sleeptime.tv_sec  = RemainingTime.tv_sec;
      sleeptime.tv_nsec = RemainingTime.tv_nsec;
    } else
    {
      perror("OS_Sleep failed");
    }
  }
  errno = iTmpErrno;
}

/*****************************************************************************/
/*! Creates thread
*     \param pvUserParam OS dependent thread parameter
*     \param ulParamSize size of buffer pointed by pvUserParam
*     \return NXT_NO_ERROR on success                                        */
/*****************************************************************************/
int32_t OS_CreateThread( void* pvUserParam, uint32_t ulParamSize)
{
  LINUX_THREAD_PARAM_T* ptLinuxThreadParam = (LINUX_THREAD_PARAM_T*)pvUserParam;
  int32_t               lRet             = -1;

  if ((ptLinuxThreadParam == NULL) || (sizeof(LINUX_THREAD_PARAM_T) != ulParamSize))
    return lRet;

  ptLinuxThreadParam->iRet = pthread_create( &ptLinuxThreadParam->tThread,
                                                &ptLinuxThreadParam->tAttr,
                                                ptLinuxThreadParam->pfnStart,
                                                ptLinuxThreadParam);

  if (ptLinuxThreadParam->iRet)
    lRet = 0;

  return lRet;
}

/*****************************************************************************/
/*! Stops and delete thread resources
*     \param pvUserParam OS dependent thread parameter
*     \return NXT_NO_ERROR on success                                        */
/*****************************************************************************/
int32_t OS_DeleteThread( void* pvUserParam)
{
  LINUX_THREAD_PARAM_T* ptLinuxThreadParam = (LINUX_THREAD_PARAM_T*)pvUserParam;
  int32_t               lRet             = -1;

  if (ptLinuxThreadParam == NULL)
    return lRet;

  pthread_join( ptLinuxThreadParam->tThread, NULL);

  return lRet;
}

/*****************************************************************************/
/*! Returns pointer to an interlocked variable
*     \return pointer to interlocked variabqle                                */
/*****************************************************************************/
void* OS_CreateInterLockedVariable(void)
{
  volatile int* ptAtomic = (volatile int*)OS_Memalloc(sizeof(volatile int));

  *ptAtomic = 0;
  return (void*)ptAtomic;
}

/*****************************************************************************/
/*! Frees the interlocked variable, previously allocated by
    OS_CreateInterLockedVariable()
*     \param pvVal Pointer to the interlocked variable allocated by
                   OS_CreateInterLockedVariable()                            */
/*****************************************************************************/
void OS_DeleteInterLockedVariable(void* pvVal)
{
  OS_Memfree( pvVal);
}

/*****************************************************************************/
/*! Increments the current value in an atomic fashion
*     \param pvVal Pointer to the interlocked variable allocated by
                   OS_CreateInterLockedVariable()
*     \return current value+1                                                */
/*****************************************************************************/
uint32_t OS_InterLockedInc( void* pvVal)
{
//  printf("Inc %d\n", *((uint32_t*)pvVal));
  return __sync_add_and_fetch((volatile int*)pvVal, 1);
}

/*****************************************************************************/
/*! Decrements the current value in an atomic fashion
*     \param pvVal Pointer to the interlocked variable allocated by
                   OS_CreateInterLockedVariable()
*     \return current value-1                                                */
/*****************************************************************************/
uint32_t OS_InterLockedDec( void* pvVal)
{
//  printf("Dec %d\n", *((uint32_t*)pvVal));
  return __sync_sub_and_fetch((volatile int*)pvVal, 1);
}

/*****************************************************************************/
/*! Returns the current value in an atomic fashion
*     \param pvVal Pointer to the interlocked variable allocated by
                   OS_CreateInterLockedVariable()
*     \return current value                                                  */
/*****************************************************************************/
uint32_t OS_InterLockedRead( void* pvVal)
{
//  printf("Read %d\n", *((uint32_t*)pvVal));
  return __sync_add_and_fetch((volatile int*)pvVal, 0);
}

/*****************************************************************************/
/*! Sets value of the current variable to the given value in an atomic fashion
*     \param pvVal Pointer to the interlocked variable allocated by
*     \return current value                                                  */
/*****************************************************************************/
uint32_t OS_InterLockedSet( void* pvVal, uint32_t ulVal)
{
  __sync_lock_test_and_set((volatile int*)pvVal, ulVal);
  __sync_lock_release ((volatile int*)pvVal);

  return ulVal;
}

