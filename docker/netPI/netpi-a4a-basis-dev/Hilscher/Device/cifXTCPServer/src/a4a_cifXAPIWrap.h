/*
   -----------------------------------------------------------------------
   --                       Ada for Automation                          --
   --                                                                   --
   --              Copyright (C) 2012-2014, Stephane LOS                --
   --                                                                   --
   -- This library is free software; you can redistribute it and/or     --
   -- modify it under the terms of the GNU General Public               --
   -- License as published by the Free Software Foundation; either      --
   -- version 2 of the License, or (at your option) any later version.  --
   --                                                                   --
   -- This library is distributed in the hope that it will be useful,   --
   -- but WITHOUT ANY WARRANTY; without even the implied warranty of    --
   -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU --
   -- General Public License for more details.                          --
   --                                                                   --
   -- You should have received a copy of the GNU General Public         --
   -- License along with this library; if not, write to the             --
   -- Free Software Foundation, Inc., 59 Temple Place - Suite 330,      --
   -- Boston, MA 02111-1307, USA.                                       --
   --                                                                   --
   -- As a special exception, if other files instantiate generics from  --
   -- this unit, or you link this unit with other files to produce an   --
   -- executable, this  unit  does not  by itself cause  the resulting  --
   -- executable to be covered by the GNU General Public License. This  --
   -- exception does not however invalidate any other reasons why the   --
   -- executable file  might be covered by the  GNU Public License.     --
   -----------------------------------------------------------------------
 */

#ifndef __A4A_CIFX_API_WRAP_H
#define __A4A_CIFX_API_WRAP_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "cifXUser.h"

  /* Global driver functions */
  int32_t     APIENTRY xDriverGetInformationWrap
    ( CIFXHANDLE  hDriver, uint32_t ulSize, void* pvDriverInfo);

  int32_t     APIENTRY xDriverEnumBoardsWrap
    ( CIFXHANDLE  hDriver, uint32_t ulBoard,
    uint32_t ulSize, void* pvBoardInfo);

  int32_t     APIENTRY xDriverEnumChannelsWrap
    ( CIFXHANDLE  hDriver, uint32_t ulBoard, uint32_t ulChannel,
    uint32_t ulSize, void* pvChannelInfo);

  /* System device depending functions */
  int32_t     APIENTRY xSysdeviceOpenWrap
    ( CIFXHANDLE  hDriver, char*   szBoard, CIFXHANDLE* phSysdevice);

  int32_t     APIENTRY xSysdeviceCloseWrap
    ( CIFXHANDLE  hDriver);

  int32_t     APIENTRY xSysdevicePutPacketWrap
    ( CIFXHANDLE  hSysdevice, CIFX_PACKET* ptSendPkt, uint32_t ulTimeout);

  int32_t     APIENTRY xSysdeviceGetPacketWrap
    ( CIFXHANDLE  hSysdevice, uint32_t ulSize, CIFX_PACKET* ptRecvPkt,
    uint32_t ulTimeout);

  int32_t     APIENTRY xSysdeviceInfoWrap
    ( CIFXHANDLE  hSysdevice, uint32_t ulCmd, uint32_t ulSize, void* pvInfo);

  int32_t     APIENTRY xSysdeviceFindFirstFileWrap
    ( CIFXHANDLE  hSysdevice, uint32_t ulChannel,
    CIFX_DIRECTORYENTRY* ptDirectoryInfo,
    PFN_RECV_PKT_CALLBACK pfnRecvPktCallback, void* pvUser);

  int32_t     APIENTRY xSysdeviceFindNextFileWrap
    ( CIFXHANDLE  hSysdevice, uint32_t ulChannel,
    CIFX_DIRECTORYENTRY* ptDirectoryInfo,
    PFN_RECV_PKT_CALLBACK pfnRecvPktCallback, void* pvUser);

  /* Channel depending functions */
  int32_t     APIENTRY xChannelOpenWrap
    ( CIFXHANDLE  hDriver,  char* szBoard,
    uint32_t ulChannel, CIFXHANDLE* phChannel);

  int32_t     APIENTRY xChannelCloseWrap
    ( CIFXHANDLE  hChannel);

  int32_t     APIENTRY xChannelInfoWrap
    ( CIFXHANDLE  hChannel, uint32_t ulSize, void* pvChannelInfo);

  int32_t     APIENTRY xChannelIOReadWrap
    ( CIFXHANDLE  hChannel, uint32_t ulAreaNumber, uint32_t ulOffset,
    uint32_t ulDataLen, void* pvData, uint32_t ulTimeout);

  int32_t     APIENTRY xChannelIOWriteWrap
    ( CIFXHANDLE  hChannel, uint32_t ulAreaNumber, uint32_t ulOffset,
    uint32_t ulDataLen, void* pvData, uint32_t ulTimeout);

  int32_t     APIENTRY xChannelCommonStatusBlockWrap
    ( CIFXHANDLE  hChannel, uint32_t ulCmd, uint32_t ulOffset,
    uint32_t ulDataLen, void* pvData);

  int32_t     APIENTRY xChannelExtendedStatusBlockWrap
    ( CIFXHANDLE  hChannel, uint32_t ulCmd, uint32_t ulOffset,
    uint32_t ulDataLen, void* pvData);

#ifdef __cplusplus
}
#endif

#endif /* __A4A_CIFX_API_WRAP_H */









